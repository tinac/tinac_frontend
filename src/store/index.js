import Vue from "vue"
import Vuex from "vuex"
import axios from "axios"
import VuexPersistence from "vuex-persist"

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

axios.defaults.baseURL = "http://192.168.1.196:8888";

const store = new Vuex.Store({
  // Actual state of the store
  state: {
    sessionToken: "",
    loginErrorMessage: "",
    registerErrorMessage: "",
    universities: [],
  },

  // Computed state of the store
  // Getters only!
  computed: {},

  // Synchronous actions
  mutations: {
    saveSessionToken(state, payload) {
      state.sessionToken = payload.sessionToken
    },
    setLoginErrorMessage(state, payload) {
      state.loginErrorMessage = payload.message
    },
    setRegisterErrorMessage(state, payload) {
      state.registerErrorMessage = payload.message
    },
    logout(state) {
      state.sessionToken = "";
    },
    saveUniversities(state, payload) {
      state.universities = payload.universities
    },
  },

  // Asynchronous actions
  actions: {
    submitLogin(context, payload) {
      return new Promise((resolve, reject) => {

        let loginToken = btoa(payload.username + ":" + payload.password);

        let options = {
          auth: {
            username: payload.username,
            password: payload.password
          }
        };

        context.commit('setLoginErrorMessage', {"message": ""});

        axios.post("/login", {}, options).then(res => {
          axios.defaults.headers.common['x-auth-token'] = res.data.token;
          context.commit('saveSessionToken', {"sessionToken": res.data.token});
          resolve(res.data);
        }).catch(err => {
          if (err.response && err.response.data && err.response.data.message) {
            context.commit('setLoginErrorMessage', {"message": err.response.data.message});
          } else if (err.message) {
            context.commit('setLoginErrorMessage', {"message": err.message});
          }

          reject(err);
        });

      });
    },
    submitRegister(context, payload) {
      return new Promise((resolve, reject) => {
        let body = {
          username: payload.username,
          password: payload.password,
        }

        context.commit('setRegisterErrorMessage', {"message": ""})

        axios.post("/register", body).then(res => {
          console.log(res);
          resolve(res);
        }).catch(err => {
          context.commit('setRegisterErrorMessage', {"message": err.response.data.message})
          reject(err);
          console.error(err);
        })
      });
    },
    fetchUniversities(context, payload) {
      return new Promise((resolve, reject) => {
        axios.get("/university").then(res => {
          context.commit('saveUniversities', {"universities": res.data.data});
          resolve(res);
        }).catch(err => {
          reject(err);
        })
      })
    },
    fetchSchools(context, payload) {
      return new Promise((resolve, reject) => {
        axios.get(payload.university.url).then(res => {
          resolve(res);
        }).catch(err => {

          reject(err);
        })
      });
    },
    fetchDegrees(context, payload) {
      return new Promise((resolve, reject) => {
        axios.get(payload.school.url).then(res => {
          resolve(res);
        }).catch(err => {
          reject(err);
        })
      })
    }
  },
  plugins: [vuexLocal.plugin]
});

axios.defaults.headers.common['x-auth-token'] = store.state.sessionToken;

export default store;
