import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
import EditCalendar from '@/pages/EditCalendar'

Vue.use(Router);

export default new Router({
  routes: [
    {path: '/', name: 'Index', component: Index},
    {path: '/login', name: 'Login', component: Login},
    {path: '/register', name: 'Register', component: Register},
    {path: '/calendar', name: 'Calendar', component: Index},
    {path: '/editCalendar', name: 'EditCalendar', component: EditCalendar}
  ]
})
